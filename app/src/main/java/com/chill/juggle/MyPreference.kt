package com.chill.juggle

import android.content.Context

class MyPreference(context: MainActivity){

    val PREFERENCENAME = "SharedPrenferenceExample"
    val PREFERENCELOGINCOUNT = "LoginCount"

    val preference = context.getSharedPreferences(PREFERENCENAME, Context.MODE_PRIVATE)

    fun getLoginCount() : Int {
        return preference.getInt(PREFERENCELOGINCOUNT,0)
    }

    fun setLoginCount(count:Int){
        val editor = preference.edit()
        editor.putInt(PREFERENCELOGINCOUNT,count)
        editor.apply()
    }
}