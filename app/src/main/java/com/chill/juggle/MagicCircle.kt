package com.chill.juggle

import android.app.AlertDialog
import android.graphics.Color
import android.os.Build
import androidx.annotation.RequiresApi


data class MagicCircle(val maxX: Int, val maxY: Int, var isFalling: Boolean= true, var cx: Float, var cy: Float) {

    var rad: Float = (50..100).random().toFloat()
    var dx = (1..7).random()
    var vitesse:Float = CustomView.DELTA
    var acceleration:Float = 0F
    var decceleration:Float = 0F
    var factor:Float = 0F
    var lose = false
    val r = (0..255).random().toFloat()
    val g = (0..255).random().toFloat()
    val b = (0..255).random().toFloat()
    val a = (75..100).random()/100.toFloat()
    @RequiresApi(Build.VERSION_CODES.O)
    val randomColor = Color.argb(a, r, g, b)


    fun fall() {
        decceleration=0F
        when {
            cx !in 0..maxX -> dx = -dx
            cy >= maxY -> lose = true
        }
        cy += vitesse
        cx += dx
        vitesse+=acceleration
        acceleration += factor
        factor+=0.001F
    }

    fun bounce() {
        acceleration=0F
        factor=0F
        when {
            cx !in 0..maxX -> dx = -dx
            cy <= 0 -> isFalling=true
        }
        cy -= vitesse
        cx+=dx
        vitesse-=decceleration
        decceleration += 0.05F
        when {
            vitesse <= 5F -> isFalling=true
        }
    }

}