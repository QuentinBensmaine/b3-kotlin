package com.chill.juggle

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_game.*
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onResume() {
        super.onResume()
        val sharedPreferences = getSharedPreferences("com.chill.juggle", Context.MODE_PRIVATE)
        val lastScore = sharedPreferences.getInt("lastScore", -1)

        if (lastScore > -1){
            score.text = "Ton dernier score est : $lastScore"
        }
    }

    fun onClick1(view: View){
        val intent = Intent(this,GameActivity::class.java)
        startActivity(intent)
    }

    fun onClick2(view: View){
        val intent = Intent(this,GameActivity2::class.java)
        startActivity(intent)
    }

    fun end() {
        val sharedPreferences = getSharedPreferences("com.chill.juggle", Context.MODE_PRIVATE)
        sharedPreferences.edit().putInt("lastScore", CustomView.jongles).apply()

        var alert = AlertDialog.Builder(this)
        alert.setTitle("GAME OVER")
        alert.setMessage("Votre score est de : " + CustomView.jongles + " jongles")
        alert.setPositiveButton("Menu") { dialog: DialogInterface?, which: Int ->
            finish()
        }
        alert.show()
    }
}
