package com.chill.juggle

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.os.Build
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import androidx.annotation.RequiresApi
import kotlinx.android.synthetic.main.activity_game2.view.*

class CustomView2 : View {
    constructor(context: Context) : this(context, null)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
    }

    override fun onDraw(canvas: Canvas?) {
        mPaint1.color = mCircle1.randomColor
        mPaint2.color = mCircle2.randomColor
        if (mCircle1.isFalling){
            mCircle1.fall()
        }else{
            mCircle1.bounce()
        }
        if(mCircle2.isFalling){
            mCircle2.fall()
        }else{
            mCircle2.bounce()
        }
        super.onDraw(canvas)
        canvas?.drawCircle(mCircle2.cx, mCircle2.cy, mCircle2.rad, mPaint1)
        canvas?.drawCircle(mCircle1.cx, mCircle1.cy, mCircle1.rad, mPaint2)
        invalidate()
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bot: Int) {
        super.onLayout(changed, left, top, right, bot)
        mCircle1 = MagicCircle(width, height,true, cx1, cy1)
        mCircle2 = MagicCircle(width, height,true, cx2, cy2)
    }

    lateinit var mCircle1: MagicCircle
    lateinit var mCircle2: MagicCircle
    private var mPaint1 = Paint()
    private var mPaint2 = Paint()
    var cx1 = (20..500).random().toFloat()
    var cy1 = (20..200).random().toFloat()
    var cx2 = (500..1000).random().toFloat()
    var cy2 = (20..200).random().toFloat()

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        if (event != null) {
            if (event.y in mCircle1.cy-mCircle1.rad..mCircle1.cy+mCircle1.rad && event.x in mCircle1.cx-mCircle1.rad..mCircle1.cx+mCircle1.rad){
                mCircle1.isFalling=false
                add()
                println("balle 1")
            }else if (event.y in mCircle2.cy-mCircle2.rad..mCircle2.cy+mCircle2.rad && event.x in mCircle2.cx-mCircle2.rad..mCircle2.cx+mCircle2.rad){
                mCircle2.isFalling=false
                add()
                println("balle 2")
                println(mCircle2.cy)
            }
        }
        return super.onTouchEvent(event)
    }

    fun add() {
        jongles += 1
        if (jongles > maxJongles){maxJongles=jongles}
        println(jongles)
        println(maxJongles)
        //Toast.makeText(context,"+1",Toast.LENGTH_SHORT).show()
        //count.text = jongles.toString()
    }


    companion object {
        val DELTA = 1F
        var jongles: Int = 0
        var maxJongles = 0
    }


}