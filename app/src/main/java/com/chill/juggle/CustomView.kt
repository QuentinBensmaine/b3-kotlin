package com.chill.juggle

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.os.Build
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import androidx.annotation.RequiresApi


class CustomView : View {
    constructor(context: Context) : this(context, null)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
    }

    override fun onDraw(canvas: Canvas?) {
        mPaint.color = randomColor
        if (!mCircle.lose) {
            if (mCircle.isFalling) {
                mCircle.fall()
            } else {
                mCircle.bounce()
            }
        }else{
            println("game over")
            gameOver = true
        }

        super.onDraw(canvas)
        canvas?.drawCircle(mCircle.cx, mCircle.cy, mCircle.rad, mPaint)
        invalidate()
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bot: Int) {
        super.onLayout(changed, left, top, right, bot)
        mCircle = MagicCircle(width, height,true, cx, cy)
    }

    lateinit var mCircle: MagicCircle
    private var mPaint = Paint()
    val r = (0..255).random().toFloat()
    val g = (0..255).random().toFloat()
    val b = (0..255).random().toFloat()
    val a = (50..100).random()/100.toFloat()
    @RequiresApi(Build.VERSION_CODES.O)
    val randomColor = Color.argb(a, r, g, b)
    var cx = (20..500).random().toFloat()
    var cy = (20..200).random().toFloat()

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        if (event != null) {
            if (event.y in mCircle.cy-mCircle.rad..mCircle.cy+mCircle.rad && event.x in mCircle.cx-mCircle.rad..mCircle.cx+mCircle.rad){
                mCircle.isFalling=false
                add()
            }
        }
        return super.onTouchEvent(event)
    }

    fun add() {
        jongles += 1
        if (jongles > maxJongles){maxJongles=jongles}
        println(jongles)
        println(maxJongles)
        //Toast.makeText(context,"+1",Toast.LENGTH_SHORT).show()
        //count1.text = jongles.toString()
    }


    companion object {
        val DELTA = 1F
        var jongles: Int = 0
        var maxJongles = 0
        var gameOver = false
    }


}